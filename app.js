var app = require('http').createServer(function(req, res) {

});
var io = require('socket.io').listen(app);
var fs = require('fs');

var request = require('request');
var path_api = "http://stc.datayet.com/";

app.listen(4050);

var users_socket = {};

io.on('connection', function (socket) {

	/*
	 * Set the user's id
	 */
	socket.on('set_me', function(data){
		users_socket[socket.id] = data.id;
	});

	/*
	 * Receives a message
	 */
	socket.on('new-message',function(data, callback){
		send_message(data.user_id, data.message, data.access_token, callback);
	});

	/*
	 * Event fired when a user disconnects
	 */
	socket.on('disconnect', function (data) {
		if(typeof users_socket[socket.id] != 'undefined'){
			delete users_socket[socket.id];
		}
	});

	/*
	 * Event fired when someone add a contact
	 */
	socket.on('added-user', function(data){
		var token = find_user(data.id);
		if(token !== false){
			io.to(token).emit('added-me', {});
		}else{

		}
	});

	/*
	 * Check users online
	 */
	socket.on('users-online', function (data) {

	});

	/*
	 * Sends and saves a mensage to a user
	 */
	function send_message(user_id, message, access_token, callback){
		request.post(path_api+'add-message',
			{
				form: {
					message:message,
					user_id:user_id,
					access_token:access_token
				}
			},
			function (error, response, body) {
				var data = JSON.parse(body);
				if(typeof data.sent != 'undefined'){
					//send the message to socket
					send_message_socket(user_id, data.message);
					callback(data.message);
				}
				return body;
			}
		);
	}

	/*
	 * Sends the message to the socket
	 */
	function send_message_socket(user, message){
		var token = find_user(user);
		if(token !== false){
			io.to(token).emit('in-message', message);
		}else{

		}
	}

	/*
	 * Tell the other users you got are offline
	 */
	function send_offline_event(user){

	}

	/*
	 * Tell the other users you are online
	 */
	function send_online_event(user){

	}

	/*
	 * Finds a user by its id
	 */
	function find_user(user_id){
		for(var i in users_socket){
			if(users_socket[i] == user_id){
				return i;
			}
		}
		return false;
	}

});