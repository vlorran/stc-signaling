var sequelize = require('../providers/sequelize.js');
var Sequelize = require('sequelize');
var Message = require('./Message.js');

var User = sequelize.define('user', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	name: Sequelize.STRING(100),
	email: Sequelize.STRING(100),
	phone: Sequelize.STRING(15),
	password: Sequelize.STRING(100)
},{
	classMethods: {

	},
	instanceMethods: {

	}
});

// User.sync();

module.exports = User;