var sequelize = require('../providers/sequelize.js');
var Sequelize = require('sequelize');
var User = require('./User.js');

var Message = sequelize.define('message', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	message: Sequelize.STRING(255),
	user_from: Sequelize.INTEGER,
	user_to: Sequelize.INTEGER,
	sent: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
},{
	classMethods: {
		
	},
	instanceMethods: {
		
	}
});

// Message.sync();

module.exports = Message;